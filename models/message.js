'use strict';
module.exports = function(sequelize, DataTypes) {
  var Message = sequelize.define('Message', {
    text: DataTypes.TEXT,
    senderId: DataTypes.INTEGER,
    recipientId: DataTypes.INTEGER,
  }, {
    classMethods: {
      associate: function(models) {
      }
    }
  });
  return Message;
};