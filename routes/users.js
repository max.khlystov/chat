var express = require('express');
var router = express.Router();
var models  = require('../models');

router.route('/')
  .get(function(req, res, next) {
    models.User.findAll()
      .then(function(users) {
         res.send(users);
      });
  })
  .post(function(req, res, next) {
     models.User.findOrCreate({
      where : {username: req.body.username}
     }).then(function(user) {
       res.send(user);
     });
  });

module.exports = router;
