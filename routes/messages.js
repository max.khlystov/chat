var express = require('express');
var router = express.Router();
var models  = require('../models');

router.route('/')
  .post(function(req, res, next) {
     models.Message.create({
      text: req.body.text,
      senderId: req.body.senderId,
      recipientId: req.body.recipientId
    }).then(function(message) {
      res.io.to(message.recipientId).emit('messages', message);
      res.io.to(message.senderId).emit('messages', message);
      res.send(message);
    });
  });

router.get('/:first_user_id/:next_user_id', function (req, res, next) {
  models.Message.findAll({
    where: {
      senderId: {
        $in: [
          req.params.first_user_id, 
          req.params.next_user_id
        ],
      },
      recipientId: {
        $in: [
          req.params.first_user_id, 
          req.params.next_user_id
        ],
      }
    }
  }).then(function(messages) {
    res.send(messages);
  });
});

module.exports = router;
