var App = new Vue({
  el: 'body',
  data: function () {
    return {
      socket: null,
      userId: null,
      username: null,
      users: [],
      messages: [],
      currentChat: null,
      msg: null
    }
  },
  ready: function() {
    
  },
  created: function() {
    var ctx = this;
    this.socket = io.connect('http://localhost:3000');
    this.socket.on('messages', function(data) {
      ctx.messages.push(data);
    });
  },
  compiled: function() {

  },
  methods: {
    sendMsg: function(event) {
      event.preventDefault();

      var ctx = this;
      $.post('/messages', {
        text: ctx.msg,
        senderId: ctx.userId,
        recipientId: ctx.currentChat.id,
      }, function(data) {
        ctx.msg = null;
      });
    },
    setCurrentChat: function(user) {
      this.currentChat  = user;
      this.fetchMessages()
    },
    fetchMessages: function() {
      var ctx = this;
      $.get('/messages/' + this.userId + '/' + this.currentChat.id, function(data) {
        ctx.messages = data;
      });
    },
    fetchUsersList: function() {
      var ctx = this;
      $.get('/users', function(data) {
        ctx.users = data;
      });
    },
    enterToChat: function() {
      var ctx = this;
      $.post('/users', {
        username: ctx.username
      }, function(data) {
        ctx.userId = data[0].id;
        ctx.socket.emit('join', {room: data[0].id});
        ctx.fetchUsersList()
      });
    }
  },
  events: {

  }
});
